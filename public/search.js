// script.js
$(document).ready(function() {
    $('#searchInput').on('input', function() {
      var query = $(this).val();
  
      $.ajax({
        type: 'POST',
        url: 'search-appointments',
        data: { query: query },
        success: function(response) {
          $('#searchResults').html(response);
        },
        error: function(error) {
          console.error('AJAX request failed: ' + error);
        }
      });
    });
  });
  