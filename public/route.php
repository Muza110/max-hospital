<?php

/**
 * Routing
 */
$router = new Core\Router();

//Register Doctors
$router->add('RD', ['controller' => 'AuthController', 'action' => 'createDoctor']);
$router->add('doctors-store', ['controller' => 'AuthController', 'action' => 'Doctorstore']);
$router->add('RU', ['controller' => 'AuthController', 'action' => 'createPatient']);
$router->add('users-store', ['controller' => 'AuthController', 'action' => 'Patientstore']);

// Add the routes
$router->add('Home', ['controller' => 'Home', 'action' => 'index']);

$router->add('login-form', ['controller' => 'AuthController', 'action' => 'loginForm']);
$router->add('login-store', ['controller' => 'AuthController', 'action' => 'loginStore']);
$router->add('logout', ['controller' => 'AuthController', 'action' => 'logOut']);
$router->add('forgot-password', ['controller' => 'Mailer', 'action' => 'resetCodeForm']);
$router->add('reset-password', ['controller' => 'Mailer', 'action' => 'sendResetCode']);


$router->add('controlls', ['controller' => 'ControllController', 'action' => 'index']);
$router->add('controlls-create', ['controller' => 'ControllController', 'action' => 'create']);
$router->add('controlls-store', ['controller' => 'ControllController', 'action' => 'store']);
$router->add('controlls-edit', ['controller' => 'ControllController', 'action' => 'edit']);
$router->add('controlls-update', ['controller' => 'ControllController', 'action' => 'update']);
$router->add('controlls-delete', ['controller' => 'ControllController', 'action' => 'destroy']);

// $router->add('users', ['controller' => 'UserController', 'action' => 'index']);
// $router->add('users-create', ['controller' => 'UserController', 'action' => 'create']);
// $router->add('users-store', ['controller' => 'UserController', 'action' => 'store']);
$router->add('users-edit', ['controller' => 'UserController', 'action' => 'edit']);
$router->add('users-update', ['controller' => 'UserController', 'action' => 'update']);
$router->add('users-delete', ['controller' => 'UserController', 'action' => 'destroy']);

$router->add('doctors', ['controller' => 'DoctorController', 'action' => 'index']);
$router->add('users', ['controller' => 'DoctorController', 'action' => 'Patientindex']);
// $router->add('doctors-create', ['controller' => 'DoctorController', 'action' => 'create']);
// $router->add('doctors-store', ['controller' => 'DoctorController', 'action' => 'store']);
$router->add('doctors-edit', ['controller' => 'DoctorController', 'action' => 'edit']);
$router->add('doctors-update', ['controller' => 'DoctorController', 'action' => 'update']);
$router->add('doctors-delete', ['controller' => 'DoctorController', 'action' => 'destroy']);

$router->add('appointments', ['controller' => 'AppointmentController', 'action' => 'index']);
$router->add('appointments-create', ['controller' => 'AppointmentController', 'action' => 'create']);
$router->add('appointments-store', ['controller' => 'AppointmentController', 'action' => 'store']);
$router->add('appointments-edit', ['controller' => 'AppointmentController', 'action' => 'edit']);
$router->add('appointments-update', ['controller' => 'AppointmentController', 'action' => 'update']);
$router->add('search-appointments', ['controller' => 'AppointmentController', 'action' =>'searchAppointments']);
$router->add('appointments-delete', ['controller' => 'AppointmentController', 'action' => 'destroy']);

$router->dispatch($_SERVER['QUERY_STRING']);


?>