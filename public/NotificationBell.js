// NotificationBell.js

import React, { useState, useEffect } from 'react';

function NotificationBell() {
  const [notifications, setNotifications] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Fetch notifications from the backend (you need to implement this)
    // For simplicity, we're using a sample data fetch here
    fetchNotifications()
      .then((data) => {
        setNotifications(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching notifications: ', error);
        setIsLoading(false);
      });
  }, []);

  const fetchNotifications = async () => {
    // You should replace this with your actual API endpoint
    const response = await fetch('your_api_endpoint_here');
    const data = await response.json();
    return data;
  };

  return (
    <div>
      <span className="notification-icon">🔔</span>
      <div className="notification-dropdown">
        <button className="notification-button">
          Notifications ({notifications.length})
        </button>
        <ul>
          {isLoading ? (
            <li>Loading notifications...</li>
          ) : (
            notifications.map((notification, index) => (
              <li key={index}>{notification.message}</li>
            ))
          )}
        </ul>
      </div>
    </div>
  );
}

export default NotificationBell;
