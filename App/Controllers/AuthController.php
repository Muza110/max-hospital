<?php

namespace App\Controllers;

use App\Helper\Session;
use App\Models\User;
use App\Models\Doctor;
use \Core\View;
use \Core\Controller;

class AuthController extends Controller
{    
   
    public function loginForm()
    {   
        $session = Session::getUserInstance();
        $message = '';
          if (!empty($session->message)) {
            $message = $session->message; } 
          //  dd($_SESSION);
        if ($session->isSignedIn()) {
            // If a user or doctor is signed in, redirect to their respective page
            if ($session->isDoctor()) {
                header('Location: controlls');
               // dd($_SESSION);
            } elseif ($session->isPatient()) {
                
                    header("location: appointments-create");
            } else {
                header('Location: appointments-create');
            }
             exit;
        } // Render the login form
        View::renderTemplate('Users/login-form.html', ['message' => $message]);
    } 

    //View per krijimin e Doctor Account.
    public function createDoctor()
    {   
        View::renderTemplate('Users/register-doctor.html');
    }

    public function Doctorstore()
    {   
       
        $doctor = new Doctor();
        $doctor->first_name = $_POST['first_name'];
        $doctor->last_name = $_POST['last_name'];
        $doctor->email = $_POST['email'];
        $doctor->password = $_POST['password'];
        $doctor->specialization = $_POST['specialization'];
        $doctor->save();
        header("Location: users"); 
       
    } 

    //View per krijimin e Patient Account.
    public function createPatient() 
    {
        View::renderTemplate('Users/register-patient.html');  
    }

    public function Patientstore()
    { // dd("User");    
      $file_tmp = $_FILES['photo']['tmp_name'];
      $file_name = time() . '-' . $_FILES['photo']['name'];
      move_uploaded_file($file_tmp, "uploads/" . $file_name);
       
      $user = new User();    
    
       $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];
        $user->email = $_POST['email'];
        $user->photo = $file_name;
        $user->password = $_POST['password'];
        $user->save();
        header("Location: users"); 
       } 
    
    
    public function loginStore()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
    
        $doctor = Doctor::where('email', $email)->where('password', $password)->latest()->first();
        $user = User::where('email', $email)->where('password', $password)->latest()->first();
    
        if ($doctor) {
            $session = Session::getDoctorInstance();
            $session->login($doctor, 'doctor');
       //     dd($session);
            header('Location: controlls');
            exit;
        } elseif ($user) {
          
            $session = Session::getUserInstance();
         //   dd($session);
            $session->login($user, 'user');
       //     dd($session);
            header('Location: appointments-create');
            exit;
        } else {
            $session = Session::getUserInstance();
            $session->message("Your email or password is incorrect");
            header('Location: login-form');
            exit;
        }
    }
    
    public function logOut()
    {   $session = Session::getUserInstance();
        if ($session->isDoctor()) {
            $session->logout('doctor');
        } else {
            $session->logout('user');
        }    header('Location: login-form');
        exit;
    }
    
    public function check() {

        View::renderTemplate("Users/account-chek.html");
    }

}
