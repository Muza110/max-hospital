<?php

namespace App\Controllers;

use App\Helper\Session;
use App\Models\Doctor;
use App\Models\User;
use \Core\View;
use \Core\Controller;

class UserController extends Controller
{
   

    public function __construct()
    {   
        $userSession = Session::getUserInstance();
        $doctorSession = Session::getDoctorInstance();
            
        if (!$userSession->isSignedIn() && !$doctorSession->isSignedIn()) {
            header('Location: login-form');
            exit;
        }
    }


  



    public function store()
    {  dd("User");    
      $file_tmp = $_FILES['photo']['tmp_name'];
      $file_name = time() . '-' . $_FILES['photo']['name'];
      move_uploaded_file($file_tmp, "uploads/" . $file_name);
       
      $user = new User();    
    
       $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];
        $user->email = $_POST['email'];
        $user->photo = $file_name;
        $user->password = $_POST['password'];
        $user->save();
        header("Location: users"); 
       } 
    
   

    public function edit()
    {
        $user = User::findOrFail($_GET['id']);
        View::renderTemplate('Users/edit.html',['users'=>$user]);
    }

    public function update()
    {
        $user = User::findOrFail($_POST['id']);
        $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];
        $user->email = $_POST['email'];
        $user->update();
        header("Location: users ");
    }

    public function destroy()
    {
        $user = User::findOrFail($_POST['id']);
        $user->delete();
        header("Location: users");
    }
}
