<?php

namespace App\Controllers;

use App\Helper\Session;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\User;
use App\Models\Controll;
use \Core\View;
use \Core\Controller;

/**/

class AppointmentController extends Controller
{
    public function __construct()
{
    $doctorSession = Session::getDoctorInstance();
    $userSession = Session::getUserInstance();
    $doki = false;
    if (!$doctorSession->isSignedIn() && !$userSession->isSignedIn()) {
        // Neither doctor nor user is signed in, redirect to login form
        header('Location: login-form');
        exit;
    }   }
   

    public function index()
{   
   $doctorSession = Session::getDoctorInstance();
    $userSession = Session::getUserInstance();

    if ($doctorSession->isSignedIn()) {
        $doki = true;
        // Get the ID of the logged-in doctor
        $doctorId = $doctorSession->getDoctorId();
        // Fetch and render only the appointments for the logged-in doctor
        $appointments = Appointment::where('doctor_id', $doctorId)
            ->leftJoin('controlls', 'appointments.id', '=', 'controlls.appointment_id')
            ->whereNull('controlls.id') // Filter out appointments with controlls
            ->with('doctor')
            ->with('user')
            ->get(); }

    if ($userSession->isSignedIn()) {
       $doki=false;
        // Get the ID of the logged-in user
        $userId = $userSession->getUserId();
        $appointments = Appointment::where('user_id', $userId)
            ->whereDoesntHave('controll')
            ->orderBy('id', 'asc')
            ->with('controll')
            ->with('user')
            ->get();
    }
     if (!$doctorSession->isSignedIn() && !$userSession->isSignedIn()) {
        header('Location: login-form');
        exit;
    }
     View::renderTemplate('Appointments/index.html', ['appointments' => $appointments,'doki'=>$doki]);
}

 
   
    public function create() {
         
    $doctors = Doctor::orderBy('first_name')->orderBy('last_name')->get();
    View::renderTemplate('Appointments/create.html', ['doctors' => $doctors]); 
    } 


    public function store()
    { // Validate the input here (e.g., check if the date is in the future)
    // Create a new appointment instance
    $appointment = new Appointment();
    $appointment->user_id = $_SESSION['userId'];
    $appointment->doctor_id = $_POST['doctor_id'];
    $appointment->appointment_date = $_POST['appointment_date'];
    $appointment->appointment_type = $_POST['appointment_type'];
    $appointment->save();
    header("Location: appointments");
    }

    public function edit()
    {
        $id = $_GET['id']; // Assuming you're passing the appointment ID in the URL
        $appointment = Appointment::findOrFail($id);
        $doctors = Doctor::get();
        $users = User::get();
         // Render the edit view with the appointment and related doctor and user
        View::renderTemplate('Appointments/edit.html', [
            'appointment' => $appointment
        ]);
    }
    

     public function update()
    {
        $appointment = Appointment::findOrFail($_POST['id']);
    //    $appointment->doctor_id = $_POST['doctor_id'];
        $appointment->appointment_date = $_POST['appointment_date'];
    //   $appointment->appointment_type = $_POST['appointment_type'];
        $appointment->update();
        header("Location: appointments");
    }

    public function searchAppointments() {
        $doctorSession = Session::getDoctorInstance();
        // Assume that doki is false by default
         $doki = false;
      // Check if a doctor is logged in and set doki to true
        if ($doctorSession->isSignedIn()) {
            $doki = true;
        }
        // Check if a search query is provided in the POST data
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            // Retrieve the search query from the POST data
            $query = $_GET['query'];
          // Perform a database query to search for appointments based on the query
            $appointments = Appointment::join('users', 'appointments.user_id', '=', 'users.id')
                ->where('users.first_name', 'LIKE', "%$query%")
                ->orWhere('users.last_name', 'LIKE', "%$query%")
                ->orderBy('users.first_name')
                ->orderBy('users.last_name')
                ->get();
        } else {
            // If no search query is provided, fetch all appointments and order them by user first and last name
            $appointments = Appointment::join('users', 'appointments.user_id', '=', 'users.id')
                ->orderBy('users.first_name')
                ->orderBy('users.last_name')
                ->get();  }
        // Render the search results view with the $appointments data and doki variable
        View::renderTemplate('Appointments/search.html', ['appointments' => $appointments, 'doki' => $doki]);
    }
    
    

    public function destroy()
    {
        $id = $_POST['id'];
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();
        header('Location: appointments');
    }
}