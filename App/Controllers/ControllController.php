<?php

namespace App\Controllers;

use App\Models\Appointment;
use App\Helper\Session;
use App\Helper\Doctor;
use App\Models\Controll;
use \Core\View;
use \Core\Controller;

/**
 * Home controller
 */
 class ControllController extends Controller
{   
   /* public function __construct()
    {  
         $doctorSession = Session::getDoctorInstance();
        $userSession = Session::getUserInstance();
    
        if (!$doctorSession->isSignedIn() && !$userSession->isSignedIn()) {
            // Neither doctor nor user is signed in, redirect to login form
            header('Location: login-form');
            exit;
        } elseif ($userSession->isSignedIn()) {
            // User is signed in, redirect to appointments-create
            header('Location: appointments-create');
            exit;
        }
 }  */
    
   /*  public function index(){   // Get the ID of the logged-in doctor
    $doctorSession = Session::getDoctorInstance();
    $doctorId = $doctorSession->getDoctorId();
    // Fetch and render only the controlls for the logged-in doctor
    $controlls = Controll::whereHas('appointment', function ($query) use ($doctorId) {
        $query->where('doctor_id', $doctorId);
    })->get();
    View::renderTemplate('Controlls/index.html', ['controlls' => $controlls]);
} */
public function index()
{   
    $doki = false;
    $userSession = Session::getUserInstance();
    $doctorSession = Session::getDoctorInstance();
    
     if ($userSession->isSignedIn()) {

        // Get the ID of the logged-in user
        $userId = $userSession->getUserId();

        // You can customize this logic for users
        $controlls = Controll::whereHas('appointment', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        })->get();
        View::renderTemplate('Controlls/index.html', ['controlls' => $controlls]);
    } elseif ($doctorSession->isSignedIn()) {
        $doki = true ;
        // Get the ID of the logged-in doctor
        $doctorId = $doctorSession->getDoctorId();
       // dd($_SESSION);
        // Fetch and render the view for doctors
        $controlls = Controll::whereHas('appointment', function ($query) use ($doctorId) {
         $query->where('doctor_id', $doctorId);
        })->get();
        View::renderTemplate('Controlls/index.html', ['controlls' => $controlls, 'doki'=>$doki]);
    } else {
        // Handle the case where neither user nor doctor is signed in, e.g., redirect to the login page
        header('Location: login-form');
        exit;
    }
}



    public function create()    {
    // Get the logged-in doctor's session instance
    $doctorSession = Session::getDoctorInstance();
    // Check if the doctor is signed in
    if ($doctorSession->isSignedIn()) {
        $doki = true;
        // Get the doctor's ID from the session
        $doctorId = $doctorSession->getDoctorId();
        // Fetch only the appointments made by the logged-in doctor
        $appointments = Appointment::where('doctor_id', $doctorId)
            ->whereDoesntHave('controll') // Filter out appointments that already have a controll record
            ->with(['user', 'doctor'])
            ->get();
         // Pass the filtered appointments to the view
        View::renderTemplate('Controlls/create.html', ['appointments' => $appointments,'doki'=>$doki]);
    } else {
        // Handle the case where the doctor is not signed in, e.g., redirect to the login page
        header('Location: login-form');
        exit; }
    }

    
        public function store() {   
    $controll = new Controll();
    $controll->appointment_id = $_POST['appointment_id'];
    $controll->diagnosis = $_POST['diagnosis'];
    $controll->prescription = $_POST['prescription'];
    $controll->save();

    // Redirect to a success page or the Controlls index
    header("Location: controlls"); }

public function edit()
{
    $id = $_GET['id'];
    $controll = Controll::findOrFail($id);
    View::renderTemplate('Controlls/edit.html', ['controll' => $controll]);
}

public function update()
{   
    $id = $_POST['id'];
    $controll = Controll::findOrFail($id);
    // Check if the logged-in user has permission to update this controll.
    // Update the controll data based on the form inputs
    $controll->diagnosis = $_POST['diagnosis'];
    $controll->prescription = $_POST['prescription'];
    
    // Save the updated controll data
    $controll->update();

    // Redirect to the controlls list or another appropriate page
    header('Location: controlls');
    exit;
}


public function destroy()
{
    $id = $_POST['id'];
    $controll = Controll::findOrFail($id);
    $controll->delete();
    header('Location: controlls');
}

}
