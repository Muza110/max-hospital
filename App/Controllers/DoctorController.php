<?php

namespace App\Controllers;

use App\Models\Appointment;
use App\Models\Doctor;
use App\Helper\Session;
use App\Models\User;
use \Core\View;
use \Core\Controller;

class DoctorController extends Controller
{

    public function __construct()
    {  
         $doctorSession = Session::getDoctorInstance();
        $userSession = Session::getUserInstance();
    
        if (!$doctorSession->isSignedIn() && !$userSession->isSignedIn()) {
            // Neither doctor nor user is signed in, redirect to login form
            header('Location: login-form');
            exit;
        } elseif ($userSession->isSignedIn()) {
            // User is signed in, redirect to appointments-create
            header('Location: appointments-create');
            exit;
        }
        // If doctor is signed in, no redirect is needed
    }

    public function index()
    {
      $doctors = Doctor::orderBy('id')->get();
      $doki = true;
      View::renderTemplate('Doctors/index.html',['doctors' => $doctors,'doki'=>$doki]);
    }
    
    public function Patientindex()  {  
    
    $users = User::orderBy('id', 'asc')->get(); 
    $doki = true;
  //  $doctorSession = Session::getDoctorInstance();
  //  $usersesion = Session::getUserInstance();
  //  dd($usersesion);
    View::renderTemplate('Users/index.html', ['users' => $users,'doki'=>$doki]);
    }


    public function edit()
    {
        $doctor = Doctor::findOrFail($_GET['id']);
        $doki = true;
        View::renderTemplate('Doctors/edit.html',['doctor'=>$doctor,'doki'=>$doki]);
    }

    public function update()
    {
        $doctor = Doctor::findOrFail($_POST['id']);
        $doctor->first_name = $_POST['first_name'];
        $doctor->last_name = $_POST['last_name'];
        $doctor->email = $_POST['email'];
        $doctor->specialization = $_POST['specialization'];
        $doctor->update();
        header("Location: doctors");
    }

    public function destroy()
    {
        $doctor = Doctor::findOrFail($_POST['id']);
        $doctor->delete();
        header("Location: doctors");
    }
}
