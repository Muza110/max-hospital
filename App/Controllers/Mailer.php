<?php
namespace App\Controllers;

use App\Models\User;
use \Core\View;
use \Core\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mailer
{
    public function forgotPasswordForm() 
    {
        View::renderTemplate('Users/forgot-password.html');  
    }

    public function sendResetCode()
    {
        $email = $_POST['email'];

        // Validate the email address
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Handle invalid email
            echo "Invalid email address";
            return;
        }

        // Check if the email exists in your database
        $user = User::where('email', $email)->first();

        if (!$user) {
            // Handle non-existing email
            echo "Email not found";
            return;
        }

        // Send the reset code to the user's email
        $this->sendResetCodeEmail($user->email);

        // Redirect to the next step (where the user enters the reset code)
        header('Location: reset-password.html');
        exit;
    }

    public function resetCodeForm()
    {
        View::renderTemplate('Users/forgot-password.html');
    }

    public function resetPassword()
    {
        $code = $_POST['code'];
        $newPassword = $_POST['new_password'];
        $confirmPassword = $_POST['confirm_password'];

        // Validate the code and passwords
        // Perform necessary checks, e.g., matching passwords, valid code, etc.

        // If validations pass, update the user's password
        // Redirect to the login page or display a success message
    }

    private function sendResetCodeEmail($email)
    {
        // Generate and store a reset code in the database for the user
        $resetCode = generateUniqueCode(); // Replace with your code generation logic
        storeResetCodeInDatabase($email, $resetCode); // Replace with your database logic

        // Send the reset code to the user's email
        $subject = "Password Reset Code";
        $body = "Your password reset code is: $resetCode";

        // Use your mailer class or function to send the email
        $this->sendEmail($email, $subject, $body);
    }

    private function sendEmail($recipient, $subject, $body)
    {
        try {
            $mail = new PHPMailer(true);

            // Mailer Settings setup
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;
            $mail->Username = 'your_gmail@gmail.com'; // Replace with your Gmail email
            $mail->Password = 'your_gmail_password'; // Replace with your Gmail password

            // Sender and recipient settings
            $mail->setFrom('your_gmail@gmail.com', 'Your Sender Name'); // Replace with your details
            $mail->addAddress($recipient);
            $mail->addReplyTo('your_gmail@gmail.com', 'Your Sender Name'); // Replace with your details

            // Setting the email content
            $mail->IsHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $body;

            $mail->send();
            echo "Email sent successfully"; // You can handle success as per your needs
        } catch (Exception $e) {
            echo "Email sending failed. Error: {$mail->ErrorInfo}"; // You can handle the error as per your needs
        }
    }
}
