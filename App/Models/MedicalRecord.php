<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model ;

class medical_record extends Model
{
    public function appointment() 
    {
        return $this->belongsTo(Appointment::class);
    }
}