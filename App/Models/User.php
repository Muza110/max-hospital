<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model ;

class User extends Model
{
    public function appointmet()
    {
    return $this->hasMany(Appointment::class);
    }
   
    public function password_reset() 
    {
        return $this->hasMany(PasswordReset::class);
    }
}