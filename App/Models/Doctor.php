<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model ;

class Doctor extends Model
{

  public function appointment() 
{
   return $this->hasMany(Appointment::class);
}

}