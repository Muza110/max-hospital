<?php
namespace App\Helper;


class Session
{
    private static $userInstance;
    private static $doctorInstance;
    private $signedIn = false;
    public $userId;
    public $doctorId;
    public $message;
   
    protected function __construct($type)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();

        }
        $this->checkLogin($type);
        $this->checkMessage();
    }

    protected function __clone() {}
                    
    /* */

    public static function getUserInstance(): Session
    {
        if (!isset(self::$userInstance)) {
            self::$userInstance = new Session('user');
        }  return self::$userInstance; 
     }

    public function getDoctorId() {
        if (isset($this->doctorId)) {
            return $this->doctorId; }
        return null;
    }


    public function getUserId() {
        if (isset($this->userId)) {
            return $this->userId; }
        return null;
    }

   
    public function isDoctor()
    {
        return isset($this->doctorId);
    }

    public function isPatient()
    {
        return isset($this->userId);
    }

    
    public static function getDoctorInstance(): Session
    {
        if (!isset(self::$doctorInstance)) {
            self::$doctorInstance = new Session('doctor');
            error_log("Doctor session instance created"); // Add this for debugging
        }
         return self::$doctorInstance; 
        }
    
    
        public function login($user, $type = 'user'){
        if ($user) {
            if ($type === 'doctor') {
                $this->doctorId = $user->id;
                $_SESSION['doctorId'] = $user->id;
                $this->signedIn = true;
                $_SESSION['user_role'] = 'doctor';
            }  else {
                $this->userId = $user->id;
                $_SESSION['userId'] = $user->id;
                $_SESSION['user_role'] = 'user';
                $this->signedIn = true;
            }
            error_log("Logged in as $type with ID: " . $user->id);
        }
    }
    

    public function checkLogin($type)
    {
        if (isset($_SESSION[$type . 'Id'])) {
            $this->{$type . 'Id'} = $_SESSION[$type . 'Id']; // Use $type . 'Id' here
            $this->signedIn = true;
        } else {
            unset($this->{$type . 'Id'});
            $this->signedIn = false;
        }   
    }
        
     public function isSignedIn() {
     return $this->signedIn; 
     }
    
    
     public function logout($type = 'user') {
        unset($_SESSION[$type . 'Id']);
        unset($_SESSION['user_role']);
      //  session_destroy(); 
        $this->{$type . 'Id'} = null;
        $this->signedIn = false;
    }
    

    public function message($msg = "") {
        if (!empty($msg)) {
            $this->message = $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }

    public function checkMessage(){
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }
}

