<?php
require "../vendor/autoload.php";
require "../Bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('controlls', function ($table) {
    $table->id();
    $table->integer('appointment_id'); // Each billing record is associated with a unique appointment.
    $table->string('diagnosis');
    $table->string('prescription');
    $table->timestamps();
});
//diagnosis TEXT , 
// prescription TEXT,