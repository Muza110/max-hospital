<?php
require "../vendor/autoload.php";
require "../Bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('appointments', function ($table) {
    $table->id();
    $table->integer('user_id');
    $table->integer('doctor_id');
    $table->datetime('appointment_date'); // No unique constraint here
    $table->enum('appointment_type', ['routine-controll', 'surgery', 'scanner']);
    $table->timestamps();
});
