<?php
require "../vendor/autoload.php";
require "../Bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('password_reset', function ($table) {
    $table->id();
    $table->unsignedBigInteger('user_id'); // Each billing record is associated with a unique appointment.
    $table->string('reset_code');
    $table->timestamp('expiration_time')->nullable();
    $table->timestamps();
});
//diagnosis TEXT , 
// prescription TEXT,