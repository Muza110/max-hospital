<?php
require "../vendor/autoload.php";
require "../Bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('doctors', function ($table) {
    $table->id();
    $table->string('first_name');
    $table->string('last_name');
    $table->string('specialization');
    $table->timestamps();
});
//Columns: doctor_id (Primary Key), user_id (Foreign Key to Users), name, specialization, license_number.c